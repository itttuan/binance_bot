import asyncio
import json
import os
import re
import requests
import sqlite3
import pandas as pd
from dotenv import load_dotenv
from datetime import datetime
from binance.spot import Spot
from binance import AsyncClient, BinanceSocketManager
from telegram.ext import Updater, CommandHandler

load_dotenv()

def get_exchange_info(pair_coins, client):
    info = client.exchange_info(pair_coins)
    return (info['symbols'][0]['baseAsset'], info['symbols'][0]['quoteAsset'])

#------------------------------------------------------------------------------------
def get_sender_fullname(update):
    sender = update.effective_user
    return f'{sender.first_name} {sender.last_name}'

#------------------------------------------------------------------------------------
def get_binance_account_info():
    
    client = Spot(key=os.getenv('BN_API_KEY'), secret=os.getenv('BN_API_SECRET'))
    account_info = client.account()

    df = pd.DataFrame(account_info['balances'])
    df.free = df.free.astype(float)
    df.locked = df.locked.astype(float)
    df['total'] = df.free + df.locked

    df = df[(df['free']>=0.0005) | (df['locked']>0)]

    result = ""
    index = 2
    total = 0
    result_usdt = ''
    for _, row in df.iterrows():
        ticker_price = 1
        asset = 'USDT'
        profit = 0
        if row["asset"] != 'USDT':
            try:
                asset = f'{row["asset"]}USDT'
                ticker_price = client.ticker_price(asset)
                ticker_price = float(ticker_price['price'])
                (avgPrice, totalQty) = get_avg_price(asset)
                if avgPrice > 0:
                    profit = round((ticker_price - avgPrice) / avgPrice * 100, 2)
            except:
                asset = f'{row["asset"]}BUSD'
                ticker_price = client.ticker_price("DFBUSD")
                ticker_price = float(ticker_price['price'])
                (avgPrice, totalQty) = get_avg_price(asset)
                if avgPrice > 0:
                    profit = round((ticker_price - avgPrice) / avgPrice * 100, 2)
                usdt_ticker_price = client.ticker_price("BUSDUSDT")
                ticker_price *= float(usdt_ticker_price['price'])

        sub_total = row["total"] * ticker_price
        sub_free = row["free"] * ticker_price
        emoji = '💤'
        if profit > 0:
            emoji = '😍'
        elif profit < 0:
            emoji = '😭'
        if sub_total >= 0.5:
            if row["asset"] == 'USDT':
                result_usdt = f'1. <b>{row["asset"]}</b> total: {sub_total:,.2f} USDT, free: {sub_free:,.2f} USDT\n'
            else:
                result += f'{index}. <b>{row["asset"]}/USDT</b> total: {sub_total:,.2f} USDT, free: {sub_free:,.2f} USDT, profit: <b>{profit}%</b> {emoji}\n'
                index += 1
            total += sub_total

    result = f"<b>Hoi Chym {account_info['accountType']}</b> ~ {total:,.2f} USDT\n" + result_usdt+ result
    return result

#------------------------------------------------------------------------------------
def say_hello(update, context):
    print ('say_hello')
    sender_fullname = get_sender_fullname(update)
    context.bot.send_message(chat_id=update.effective_chat.id, text=f'Hi! Chym <b>{sender_fullname}</b>', parse_mode='html') 

#------------------------------------------------------------------------------------
def get_balance(update, context):
    print ('get_balance')
    data = get_binance_account_info()
    context.bot.send_message(chat_id=update.effective_chat.id, text=data, parse_mode='html') 

#------------------------------------------------------------------------------------
def get_price(update, context):
    print ('get_price')
    client = Spot(key=os.getenv('BN_API_KEY'), secret=os.getenv('BN_API_SECRET'))
    if (len(context.args) == 0):
        account_info = client.account()
        df = pd.DataFrame(account_info['balances'])
        df.free = df.free.astype(float)
        df.locked = df.locked.astype(float)

        df = df[(df['free']>=0.0005) | (df['locked']>0)]

        coins = df.asset.tolist()
        coins.remove('USDT')
    else:
        coins = context.args

    for coin in coins:
        coin = coin.upper()

        try:
            try:
                quote_asset = 'USDT'
                ticker_price = client.ticker_price(f'{coin}USDT')
            except:
                quote_asset = 'BUSD'
                ticker_price = client.ticker_price(f'{coin}BUSD')
            
            ticker_price = float(ticker_price['price'])
            ticker_price_str = f"{ticker_price:,.2f}" if ticker_price > 1 else f"{ticker_price:,.5f}"

            result = f"<b>-- LASTEST PRICE --</b>\n<b>{coin}</b>: ~ {ticker_price_str} {quote_asset}"
        except:
            result = f"Khong the tra cuu coin <b>{coin}</b>"

        context.bot.send_message(chat_id=update.effective_chat.id, text=result, parse_mode='html') 

    sender_fullname = get_sender_fullname(update)
    context.bot.send_message(chat_id=update.effective_chat.id, text=f'--- Xong nha Chym <b>{sender_fullname}</b> ---', parse_mode='html') 
    
#------------------------------------------------------------------------------------
def get_pending_orders(update, context):
    print ('get_pending_orders')
    client = Spot(key=os.getenv('BN_API_KEY'), secret=os.getenv('BN_API_SECRET'))
    data = client.get_open_orders()
    if len(data) == 0:
        context.bot.send_message(chat_id=update.effective_chat.id, text='No order...', parse_mode='html') 
        return

    result = "<b>--PENDING ORDERS ne CHYM</b>--\n"
    context.bot.send_message(chat_id=update.effective_chat.id, text=result, parse_mode='html') 
    for order in data:
        price = float(order['price'])
        origQty = float(order['origQty'])
        total = price * origQty
        
        (base_asset, quote_asset) = get_exchange_info(order['symbol'], client=client)
        price_str = f"{price:,.2f}" if price > 1 else f"{price:,.5f}"

        result = f"<b>{order['side']}</b> - <b>{order['symbol']}</b>\n"
        result += f"- Price: <b>{price_str}</b> {quote_asset}\n"
        result += f"- Quantity: <b>{origQty:,.1f}</b> {base_asset}\n"
        result += f"- Total: <b>{total:,.2f}</b> {quote_asset}"

        context.bot.send_message(chat_id=update.effective_chat.id, text=result, parse_mode='html') 
    
    sender_fullname = get_sender_fullname(update)
    context.bot.send_message(chat_id=update.effective_chat.id, text=f'--- Xong nha Chym <b>{sender_fullname}</b> ---', parse_mode='html') 

#------------------------------------------------------------------------------------
def insert_db_with_history_orders(client, cur):
    print ('init_database_with_history_order')
    account_info = client.account()
    df = pd.DataFrame(account_info['balances'])
    df.free = df.free.astype(float)
    df.locked = df.locked.astype(float)
    df['total'] = df.free + df.locked

    df = df[(df['free']>=0.0005) | (df['locked']>0)]

    for _, row in df.iterrows():
        ticker_price = 1
        base = row["asset"]
        quote = 'USDT'
        if row["asset"] != 'USDT':
            try:
                asset = f'{base}USDT'
                # Check symboy with USDT
                ticker_price = client.ticker_price(asset)
            except:
                asset = f'{base}BUSD'
                quote = 'BUSD'
                # Check symboy with BUSD
                ticker_price = client.ticker_price(asset)

            ticker_price = float(ticker_price['price'])
            sub_total = row["total"] * ticker_price
            if sub_total >= 0.5:
                orders = client.get_orders(asset)
                orders_df = pd.DataFrame(orders)
                orders_df = orders_df[orders_df.status == 'FILLED']

                preQty = 0
                prePrice = 0
                for _, order_row in orders_df.iterrows():
                    quantity = float(order_row['origQty'])
                    price = float(order_row['price'])

                    side = order_row["side"]
                    if side == 'BUY':
                        totalQty = preQty + quantity
                        avgPrice = ((price * quantity) + (prePrice * preQty)) / totalQty
                    else:
                        totalQty = preQty - quantity
                        if totalQty * prePrice < 0.5:
                            avgPrice = 0
                            totalQty = 0
                        else:
                            avgPrice = prePrice

                    avgPrice = round(avgPrice, 5)
                    prePrice = avgPrice
                    preQty = totalQty

                    cur.execute(f"""
                    INSERT INTO logs(asset, base_coin, quote_coin, quantity, usdt_price, total_quantity, usdt_avg_price, side, created_at) 
                    VALUES('{asset}', '{base}', '{quote}', {quantity}, {price}, {totalQty}, {avgPrice}, '{side}', {order_row['time']});
                    """)
    print ('Done...')

#------------------------------------------------------------------------------------
def init_database():
    conn = sqlite3.connect('logs.db')
    print ("Opened database successfully");
    try:
        conn.execute('''CREATE TABLE logs (
                         id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                         asset          TEXT        NOT NULL,
                         base_coin      TEXT        NOT NULL,
                         quote_coin     TEXT        NOT NULL,
                         quantity       DOUBLE      NOT NULL,
                         usdt_price     DOUBLE      NOT NULL,
                         total_quantity DOUBLE      NOT NULL,
                         usdt_avg_price DOUBLE      NOT NULL,
                         side           VARCHAR(4)  NOT NULL,
                         created_at     DATETIME);''')

        print ("Table created successfully");
        client = Spot(key=os.getenv('BN_API_KEY'), secret=os.getenv('BN_API_SECRET'))
        insert_db_with_history_orders(client, conn.cursor())
        conn.commit()
    except:
        print ("Database alredy exists");
        return 'Database alredy exists'
        
    conn.close()
    return 'Create database from order histories successfully'
#------------------------------------------------------------------------------------
def get_avg_price(asset: str) -> float:
    conn = sqlite3.connect('logs.db')
    cur = conn.cursor()   
    cur.execute(f"SELECT * FROM logs WHERE asset='{asset}' ORDER BY id DESC")

    row = cur.fetchone()
    if row:
        avgPrice = row[7]
        totalQty = row[6]
    else:
        avgPrice = 0
        totalQty = 0
        
    conn.close()
    return (avgPrice, totalQty)

#------------------------------------------------------------------------------------
def init_database_command(update, context):
    print ('init_database')
    result = init_database()
    context.bot.send_message(chat_id=update.effective_chat.id, text=result, parse_mode='html') 

#------------------------------------------------------------------------------------
def fix_float(float_num):
    regex = r"(\.\d+?)0+\b"
    subst = "\\1"
    result = re.sub(regex, subst, str(float_num), 0)
    return result

#------------------------------------------------------------------------------------
def send_telegram(text):
    try:
        dt = {'chat_id': os.getenv('CHAT_ID'), 'text': text, 'parse_mode': 'html'}
        res = requests.get(f"https://api.telegram.org/bot{os.getenv('TLG_BOT_TOKEN')}/sendMessage", params=dt)
        ok = res.json()
        if not ok['ok']:
            print(f"Error From telegram: {ok}")
    except Exception as E:
        print(f"Exception on processing send_telegram https requests: {str(E)}")
        
#------------------------------------------------------------------------------------
def process_message(json_data):
    try:
        event_type = json_data['e']

        if event_type == 'executionReport':
            symbol = json_data['s']
            client = Spot(key=os.getenv('BN_API_KEY'), secret=os.getenv('BN_API_SECRET'))
            (base_asset, quote_asset) = get_exchange_info(symbol, client=client)
            price = json_data['p']
            quantity = fix_float(json_data['q'])
            side = json_data['S']
            order_id = json_data['i']
            order_status = json_data['X']
            last_trade_quantity = fix_float(json_data['l'])
            filled_qty = fix_float(json_data['z'])
            order_type = json_data['o']
            lprice = json_data['L']

            if order_type == "MARKET":fprice=lprice
            else:fprice=price

            if order_status == 'NEW':
                txt = f"<b>Spot {side} {order_type} Order CREATED\n" \
                      f"Symbol - {symbol}\nPrice - {fprice} {quote_asset}\n" \
                      f"Quantity - {quantity} {base_asset}\n" \
                      f"Order ID - #ID{order_id}</b>"

            elif order_status == 'CANCELED':
                txt = f"<b>Spot {side} {order_type} Order CANCELED\n" \
                      f"Symbol - {symbol}\nPrice - {fprice} {quote_asset}\n" \
                      f"Quantity - {quantity} {base_asset}\n" \
                      f"Order ID - #ID{order_id}</b>"

            elif order_status == 'PARTIALLY_FILLED':
                txt = f"<b>Spot {side} {order_type} Order PARTIALLY FILLED\n" \
                      f"Symbol - {symbol}\nPrice - {lprice} {quote_asset}\nLast Filled - {last_trade_quantity} {quote_asset}\n" \
                      f"Total Qty - {quantity} {base_asset}\nTotal Filled - {filled_qty} {quote_asset}\nTotal Remains - " \
                      f"{float(quantity) - float(filled_qty)} {base_asset}\nOrder ID - #ID{order_id}</b>"

            elif order_status == 'FILLED':
                price = float(json_data['Z']) / float(json_data['z'])
                txt = f"<b>Spot {side} {order_type} Order FULLY FILLED\n" \
                      f"Symbol - {symbol}\nAvg. Price - {fix_float(price)} {quote_asset}\n" \
                      f"Filled - {filled_qty} {base_asset}\nOrder ID - #ID{order_id}</b>"
                    
                # Write to db
                price = round(price, 5)
                filled_qty = float(filled_qty)
                (avgPrice, totalQty) = get_avg_price(symbol)
                if side == "BUY":
                    avgPrice = (filled_qty * price + totalQty * avgPrice) / (totalQty + filled_qty)
                    totalQty = totalQty + filled_qty
                elif side == "SELL":
                    totalQty = totalQty - filled_qty
                    if totalQty * avgPrice < 0.5:
                        avgPrice = 0
                        totalQty = 0
                avgPrice = round(avgPrice, 5)
                conn = sqlite3.connect('logs.db')
                with conn:
                    cur = conn.cursor()
                    cur.execute(f"""
                    INSERT INTO logs(asset, base_coin, quote_coin, quantity, usdt_price, total_quantity, usdt_avg_price, side, created_at) 
                    VALUES('{symbol}', '{base_asset}', '{quote_asset}', {filled_qty}, {price}, {totalQty}, {avgPrice}, '{side}', '{datetime.utcnow()}');
                    """)
                conn.close()
            else:
                txt = f"<b>Spot {side} {order_type} Order {order_status}\n" \
                      f"Symbol - {symbol}\nPrice - {fprice} {base_asset}\n" \
                      f"Quantity - {quantity} {quote_asset}\n" \
                      f"Order ID - #ID{order_id}</b>"
            send_telegram(txt)

    except Exception as E:
        ee = str(
            f"In spot, Exception found on processed message: {str(E)}")
        print(ee)
        send_telegram(ee)

#------------------------------------------------------------------------------------
async def spot_user(client):
    updater = Updater(token=os.getenv('TLG_BOT_TOKEN'))
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("hi", say_hello))
    dispatcher.add_handler(CommandHandler('balance', get_balance))
    dispatcher.add_handler(CommandHandler('p', get_price, pass_args=True))
    dispatcher.add_handler(CommandHandler('po', get_pending_orders))
    dispatcher.add_handler(CommandHandler('db', init_database_command))

    print ('bot is starting...')
    send_telegram('Chym Bot is starting...')
    updater.start_polling()

    bm = BinanceSocketManager(client, user_timeout=1700)  
    print ('bot is ready...')
    send_telegram('Chym Bot is ready...')

    async with bm.user_socket() as stream:
        while True:
            res = await stream.recv()
            if res is not None and "e" in res:
                process_message(res)
                print(json.dumps(res, indent=2))
            else:
                print(res)

#------------------------------------------------------------------------------------
async def hook_event():
    client = await AsyncClient.create(api_key=os.getenv('BN_API_KEY'), api_secret=os.getenv('BN_API_SECRET'))
    await spot_user(client=client)

#------------------------------------------------------------------------------------
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(hook_event())