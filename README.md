# README #
Bot for getting account info from Binance

### Required ###
python 3.8 or later

### Install dependencies ###
pip install -r requirements.txt

### Edit .env ###
TLG_BOT_TOKEN = '<your-telegram-bot-token>'
BN_API_KEY = '<your-binance-api-key>'
BN_API_SECRET = '<your-binance-api-secret>'

### Run bot ###
python bot.py